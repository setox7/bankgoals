package com.pointtrip.bankgoals.model;

/**
 * Created by krusher7 on 11/26/2016.
 */

public class Goals {
    private String title, description, level, points;
    public Goals() {
    }

    public Goals(String title, String description, String level, String points) {
        this.title = title;
        this.description = description;
        this.level = level;
        this.points = points;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
