package com.pointtrip.bankgoals.model;

/**
 * Created by krusher7 on 11/26/2016.
 */

public class Reward {

    String name;
    String Brand;
    String category;
    boolean isWishList;
    int stock;

    public Reward() {
    }

    public Reward(String name, String brand, String category,  int stock, boolean wishlist) {
        this.name = name;
        Brand = brand;
        this.category = category;
        this.isWishList = wishlist;
        this.stock = stock;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public boolean isWishlist() {
        return isWishList;
    }

    public void setWishlist(boolean wishlist) {
        this.isWishList = wishlist;
    }
}
