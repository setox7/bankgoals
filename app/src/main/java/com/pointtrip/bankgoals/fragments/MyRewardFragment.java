package com.pointtrip.bankgoals.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointtrip.bankgoals.R;
import com.pointtrip.bankgoals.adapter.GoalAdapter;
import com.pointtrip.bankgoals.adapter.MyRewardAdapter;
import com.pointtrip.bankgoals.model.Goals;
import com.pointtrip.bankgoals.model.MyRewards;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krusher7 on 11/26/2016.
 */
public class MyRewardFragment extends Fragment {

    Context mContext;
    RecyclerView rv_myrewards;
    MyRewardAdapter adapter;
    List<MyRewards> myrewardList = new ArrayList<>();
    private View view;
    public MyRewardFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_goal,container,false);

        rv_myrewards = (RecyclerView) view.findViewById(R.id.rv_goals);

        adapter = new MyRewardAdapter(myrewardList);
        rv_myrewards.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rv_myrewards.setAdapter(adapter);

        prepareData();

        return view;
    }
    private void prepareData() {
        MyRewards myRewards = new MyRewards("Title","","",10,false);
        myrewardList.add(myRewards);

        myRewards = new MyRewards("My Reward 2","","",1,false);
        myrewardList.add(myRewards);

        myRewards = new MyRewards("My Reward","","",1,false);
        myrewardList.add(myRewards);

        myRewards = new MyRewards("My Reward 2","","",1,true);
        myrewardList.add(myRewards);
        myRewards = new MyRewards("My Reward","","",1,false);
        myrewardList.add(myRewards);

        myRewards = new MyRewards("My Reward 2","","",2,true);
        myrewardList.add(myRewards);

        adapter.notifyDataSetChanged();
    }
}
