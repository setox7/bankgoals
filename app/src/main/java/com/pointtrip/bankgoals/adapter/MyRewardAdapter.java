package com.pointtrip.bankgoals.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pointtrip.bankgoals.R;
import com.pointtrip.bankgoals.model.Goals;
import com.pointtrip.bankgoals.model.MyRewards;

import java.util.List;


/**
 * Created by krusher7 on 11/26/2016.
 */
public class MyRewardAdapter extends RecyclerView.Adapter<MyRewardAdapter.ViewHolder> {

    private List<MyRewards> myRewardList;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_goal_title;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_goal_title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public MyRewardAdapter(List<MyRewards> myRewardList) {
        this.myRewardList = myRewardList;
    }

    @Override
    public MyRewardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goal
                ,parent,false);
        return new MyRewardAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyRewardAdapter.ViewHolder holder, int position) {
        MyRewards rewards = myRewardList.get(position);
        holder.tv_goal_title.setText(rewards.getName());
    }

    @Override
    public int getItemCount() {
        return myRewardList.size();
    }
}