package com.pointtrip.bankgoals;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.pointtrip.bankgoals.adapter.ViewPagerAdapter;
import com.pointtrip.bankgoals.fragments.GoalFragment;
import com.pointtrip.bankgoals.fragments.MyRewardFragment;
import com.pointtrip.bankgoals.fragments.RewardListFragment;

import java.util.ArrayList;

public class LandingActivity extends AppCompatActivity {

    public static final String TAG = LandingActivity.class.getSimpleName();

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        toolbar();
        setUpView();
    }

    private void setUpView() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setUpViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setUpViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new GoalFragment(), "ONE");
        adapter.addFragment(new MyRewardFragment(), "TWO");
        adapter.addFragment(new RewardListFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }

    private void toolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}
