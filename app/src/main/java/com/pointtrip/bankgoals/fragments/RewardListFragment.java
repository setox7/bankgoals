package com.pointtrip.bankgoals.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointtrip.bankgoals.R;
import com.pointtrip.bankgoals.adapter.GoalAdapter;
import com.pointtrip.bankgoals.adapter.RewardListAdapter;
import com.pointtrip.bankgoals.model.Goals;
import com.pointtrip.bankgoals.model.Reward;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krusher7 on 11/26/2016.
 */
public class RewardListFragment extends Fragment {

    Context mContext;
    RecyclerView rv_rewards;
    RewardListAdapter adapter;
    List<Reward> rewardsList = new ArrayList<>();
    private View view;
    public RewardListFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_goal,container,false);

        rv_rewards = (RecyclerView) view.findViewById(R.id.rv_goals);

        adapter = new RewardListAdapter(rewardsList);
        rv_rewards.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rv_rewards.setAdapter(adapter);

        prepareData();

        return view;
    }
    private void prepareData() {
        Reward reward = new Reward("Title","","",2,true);
        rewardsList.add(reward);

        reward = new Reward("Title 2","","",3,false);
        rewardsList.add(reward);

        reward = new Reward("Title","","",10,true);
        rewardsList.add(reward);

        reward = new Reward("Title 2","","",12,false);
        rewardsList.add(reward);
        reward = new Reward("Title","","",13,true);
        rewardsList.add(reward);

        reward = new Reward("Title 2","","",14,false);
        rewardsList.add(reward);

        adapter.notifyDataSetChanged();
    }

}