package com.pointtrip.bankgoals.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pointtrip.bankgoals.R;
import com.pointtrip.bankgoals.model.Goals;
import com.pointtrip.bankgoals.model.MyRewards;
import com.pointtrip.bankgoals.model.Reward;

import java.util.List;

/**
 * Created by krusher7 on 11/26/2016.
 */
public class RewardListAdapter extends RecyclerView.Adapter<RewardListAdapter.ViewHolder>{

    private List<Reward> rewardsList;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_goal_title;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_goal_title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public RewardListAdapter(List<Reward> rewardsList) {
        this.rewardsList = rewardsList;
    }

    @Override
    public RewardListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goal
                ,parent,false);
        return new RewardListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RewardListAdapter.ViewHolder holder, int position) {
        Reward reward = rewardsList.get(position);
        holder.tv_goal_title.setText(reward.getName());
    }

    @Override
    public int getItemCount() {
        return rewardsList.size();
    }
}
