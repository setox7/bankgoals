package com.pointtrip.bankgoals.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointtrip.bankgoals.R;
import com.pointtrip.bankgoals.adapter.GoalAdapter;
import com.pointtrip.bankgoals.model.Goals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by krusher7 on 11/26/2016.
 */
public class GoalFragment extends Fragment {

    Context mContext;
    RecyclerView rv_goals;
    GoalAdapter adapter;
    List<Goals> goalList = new ArrayList<>();
    private View view;

    public GoalFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_goal,container,false);

        rv_goals = (RecyclerView) view.findViewById(R.id.rv_goals);

        adapter = new GoalAdapter(goalList);
        rv_goals.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rv_goals.setAdapter(adapter);

        prepareData();

        return view;
    }

    private void prepareData() {
        Goals goal = new Goals("Title","","","");
        goalList.add(goal);

        goal = new Goals("Title 2","","","");
        goalList.add(goal);

        goal = new Goals("Title","","","");
        goalList.add(goal);

        goal = new Goals("Title 2","","","");
        goalList.add(goal);
        goal = new Goals("Title","","","");
        goalList.add(goal);

        goal = new Goals("Title 2","","","");
        goalList.add(goal);

        adapter.notifyDataSetChanged();
    }
}
