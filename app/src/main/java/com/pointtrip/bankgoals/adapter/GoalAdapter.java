package com.pointtrip.bankgoals.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pointtrip.bankgoals.R;
import com.pointtrip.bankgoals.model.Goals;

import java.util.List;

/**
 * Created by krusher7 on 11/26/2016.
 */
public class GoalAdapter extends RecyclerView.Adapter<GoalAdapter.ViewHolder>{

    private List<Goals> goalList;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_goal_title;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_goal_title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public GoalAdapter(List<Goals> goalList) {
        this.goalList = goalList;
    }

    @Override
    public GoalAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goal
        ,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GoalAdapter.ViewHolder holder, int position) {
        Goals goals = goalList.get(position);
        holder.tv_goal_title.setText(goals.getTitle());
    }
    @Override
    public int getItemCount() {
        return goalList.size();
    }
}
