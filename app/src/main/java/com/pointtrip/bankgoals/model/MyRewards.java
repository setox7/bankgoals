package com.pointtrip.bankgoals.model;

/**
 * Created by krusher7 on 11/26/2016.
 */

public class MyRewards {
    String name;
    String Brand;
    String category;
    int stock;
    boolean isRedeem;

    public MyRewards(String name, String brand, String category, int stock, boolean isRedeem) {
        this.name = name;
        Brand = brand;
        this.category = category;
        this.stock = stock;
        this.isRedeem = isRedeem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public boolean isRedeem() {
        return isRedeem;
    }

    public void setRedeem(boolean redeem) {
        isRedeem = redeem;
    }
}
